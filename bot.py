# -*- coding: utf-8 -*-

import logging
import sys
import psycopg2
import urlparse
import hashlib

from functools import partial
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters

from changer import change
from admin import admin
from basic import start, error, reply, hlp
from eml import init, send_data
from enc import decode, encde
from form_panel import form


class Bot:
    def __init__(self, token, port, url, db_url, slted, salt, mail, ps):
        self.slted = slted
        self.salt = salt
        self.mail = mail
        self.pwd = ps

        url1 = urlparse.urlparse(db_url)
        dbname = url1.path[1:]
        user = url1.username
        password = url1.password
        host = url1.hostname
        port1 = url1.port

        self.db = psycopg2.connect(
            dbname=dbname,
            user=user,
            password=password,
            host=host,
            port=port1
        )
        self.updater = Updater(token)

        self.updater.start_webhook(listen="0.0.0.0",
                                   port=port,
                                   url_path=token)
        self.updater.bot.setWebhook(url + token)

        j = self.updater.job_queue
        w = partial(send_data, db=self)
        w.__name__ = "send"
        j.run_repeating(w, 300, 300)
        j.start()

        self.updater.dispatcher.add_handler(CommandHandler('start', start))
        self.updater.dispatcher.add_handler(CommandHandler('help', hlp))
        self.updater.dispatcher.add_handler(CommandHandler('admin', partial(admin, db=self)))
        self.updater.dispatcher.add_handler(CommandHandler('form', partial(form, db=self)))
        self.updater.dispatcher.add_handler(CallbackQueryHandler(partial(change, db=self)))
        self.updater.dispatcher.add_error_handler(error)
        self.updater.dispatcher.add_handler(MessageHandler(Filters.text, partial(reply, db=self)))

    def run(self):
        self.updater.idle()

    def save_message(self, chart_id, message_id, tp=0):
        try:
            cur = self.db.cursor()
            cur.execute("INSERT INTO clean VALUES (%s, %s, %s);", (chart_id, message_id, tp))
            self.db.commit()
            cur.close()
        except:
            e = sys.exc_info()
            logging.error(e)

    def delete_message(self, chart_id, tp=0):
        try:
            cur = self.db.cursor()
            cur.execute("DELETE FROM clean WHERE chat_id = %s and typ = %s", (chart_id, tp))
            self.db.commit()
            cur.close()
        except:
            e = sys.exc_info()
            logging.error(e)

    def create_form(self, chart_id, code):
        try:
            cur = self.db.cursor()
            cur.execute("INSERT INTO forms VALUES (%s, -1, 'none', %s, 'none');", (code, chart_id))
            self.db.commit()
            cur.close()
        except:
            e = sys.exc_info()
            logging.error(e)

    def set_rate(self, chart_id, rate):
        try:
            cur = self.db.cursor()
            cur.execute("UPDATE forms SET rate = %s where chat_id = %s;", (rate, chart_id))
            self.db.commit()
            cur.close()
        except:
            e = sys.exc_info()
            logging.error(e)

    def save_form(self, chart_id, comm):
        try:
            cur = self.db.cursor()
            cur.execute("UPDATE forms SET com = %s where chat_id = %s", (comm, chart_id))
            self.db.commit()
            cur.close()
        except:
            e = sys.exc_info()
            logging.error(e)

    def save_name(self, chart_id, name):
        try:
            cur = self.db.cursor()
            cur.execute("UPDATE forms SET name = %s, chat_id = %s where chat_id = %s", (name, -1, chart_id))
            cur.execute("DELETE FROM clean WHERE chat_id = %s", (chart_id,))
            self.db.commit()
            cur.close()
        except:
            e = sys.exc_info()
            logging.error(e)

    def get_from(self, chart_id):
        try:
            cur = self.db.cursor()
            cur.execute("select message_id FROM clean WHERE chat_id = %s and typ = %s", (chart_id, 1))
            st = int(cur.fetchall()[0][0])
            cur.close()
            return st
        except:
            e = sys.exc_info()
            logging.error(e)
        return None

    def form_answer(self, chart_id):
        try:
            cur = self.db.cursor()
            cur.execute("select typ FROM clean WHERE chat_id = %s", (chart_id,))
            res = int(cur.fetchall()[0][0])
            cur.close()
            return res
        except:
            e = sys.exc_info()
            logging.error(e)
        return -1

    def valid(self, str):
        res = hashlib.sha256(str + self.salt).hexdigest()
        return res == self.slted

    def add_teacher(self, mail):
        try:
            cur = self.db.cursor()
            cur.execute("select max(code) FROM teacher")
            st = int(cur.fetchall()[0][0]) + 1
            cur.execute("INSERT INTO teacher VALUES(%s, %s)", (mail, st))
            self.db.commit()
            cur.close()
            init(self, mail, st)
            return encde(st)
        except:
            e = sys.exc_info()
            logging.error(e)
        return "-1"

    def del_teacher(self, code):
        try:
            cur = self.db.cursor()
            cur.execute("DELETE FROM teacher WHERE code = %s", (decode(code),))
            self.db.commit()
            cur.close()
        except:
            e = sys.exc_info()
            logging.error(e)

    def valid_code(self, code):
        try:
            cur = self.db.cursor()
            cur.execute("select count(*) FROM teacher where code = %s", (decode(code),))
            st = int(cur.fetchall()[0][0])
            cur.close()
            return st == 1
        except:
            e = sys.exc_info()
            logging.error(e)
        return False
