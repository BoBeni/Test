# -*- coding: utf-8 -*-
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from const import Constant


def get_reply_markup(end):
    keyboard = [[InlineKeyboardButton("1", callback_data='1' + end),
                 InlineKeyboardButton("2", callback_data='2' + end),
                 InlineKeyboardButton("3", callback_data='3' + end),
                 InlineKeyboardButton("4", callback_data='4' + end),
                 InlineKeyboardButton("5", callback_data='5' + end)]]

    return InlineKeyboardMarkup(keyboard)


def form(bot, update, db):
    if update.message.text == "/form":
        update.message.reply_text("Введи номер учителя - ****")
        db.save_message(update.message.chat.id, update.message.message_id)
    else:
        code = int(update.message.text[6:])
        if db.valid_code(code):
            db.delete_message(update.message.chat.id)
            db.create_form(update.message.chat.id, code)
            update.message.reply_text('Оцени урок:', reply_markup=get_reply_markup('rate'))
        else:
            update.message.reply_text("Неверный номер. Начни занаво.")


def get_reply_anon():
    keyboard = [[InlineKeyboardButton("Да, конечно", callback_data='anon_yes'),
                 InlineKeyboardButton("Нет, спасибо", callback_data='anon_no')]]

    return InlineKeyboardMarkup(keyboard)


def get_reply_wrong():
    keyboard = [[InlineKeyboardButton(Constant.get_end('end_boring'), callback_data='end_boring')],
                [InlineKeyboardButton(Constant.get_end('end_easy'), callback_data='end_easy')],
                [InlineKeyboardButton(Constant.get_end('end_difficult'), callback_data='end_difficult')],
                [InlineKeyboardButton(Constant.get_end('end_lost'), callback_data='end_lost')],
                [InlineKeyboardButton(Constant.get_end('end_other'), callback_data='end_other')]]

    return InlineKeyboardMarkup(keyboard)
