# -*- coding: utf-8 -*-

import logging

from telegram import InlineKeyboardButton, InlineKeyboardMarkup


def get_admin_markup():
    keyboard = [[InlineKeyboardButton("Добавить учителя", callback_data='add_admin'),
                 InlineKeyboardButton("Удалить учителя", callback_data='del_admin'),
                 InlineKeyboardButton("Выход", callback_data='exit_admin')]]

    return 'Меню', InlineKeyboardMarkup(keyboard)


def admin(bot, update, db):
    if update.message.text == "/admin":
        update.message.reply_text("Введи пароль:")
        db.save_message(update.message.chat_id, update.message.message_id, 2)
    else:
        db.delete_message(update.message.chat.id, 2)
        ps = update.message.text[7:]
        if db.valid(ps):
            text, markup = get_admin_markup()
            update.message.reply_text(text=text, reply_markup=markup)
        else:
            update.message.reply_text(text="Неверный пароль")
