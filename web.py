# -*- coding: utf-8 -*-

import os
from bot import Bot


token = os.environ.get("token")
port = int(os.environ.get('PORT', '5000'))
url = os.environ.get("URL")
db = os.environ.get("DATABASE_URL")
slted = os.environ.get("SALTED")
salt = os.environ.get("SALT")
mail = os.environ.get("mail")
ps = os.environ.get("ps")

bot = Bot(token, port, url, db, slted, salt, mail, ps)
bot.run()
