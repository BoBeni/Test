# -*- coding: utf-8 -*-

import os
import csv
import time
import logging
import sys
import numpy as np

from smtplib import SMTP
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.encoders import encode_base64

from enc import encde


def send_data(bot, job, db):
    try:
        cur = db.db.cursor()

        cur.execute("select * FROM teacher")
        users = cur.fetchall()
        mails = np.array(users[0])
        codes = np.array(users[1])

        today = (time.strftime("%m/%d/%Y"))
        mail_server = SMTP("smtp.gmail.com", 587)
        gmail_user = db.db.mail
        gmail_pwd = db.db.pwd

        logging.error(mails)
        logging.error(codes)

        for i in np.arange(mails.size):
            cur.execute("select * FROM forms where code = %s", (codes[i]))
            res = cur.fetchall()
            fp = open("/tmp/" + encde(codes[i]) + ".csv", 'w')
            attach_file = csv.writer(fp)
            attach_file.writerows(res)
            fp.close()

            msg = MIMEMultipart()
            msg['From'] = gmail_user
            msg['To'] = ", ".join([mails[i]])
            msg['Subject'] = "Feedback from %s" % today

            part = MIMEBase('application', "octet-stream")
            part.set_payload(open("/tmp/" + encde(codes[i]) + ".csv", "rb").read())
            encode_base64(part)

            part.add_header('Content-Disposition', 'attachment; filename="/tmp/file_name.csv"')

            msg.attach(part)

            mail_server.ehlo()
            mail_server.starttls()
            mail_server.ehlo()

            text = msg.as_string()
            mail_server.login(gmail_user, gmail_pwd)
            mail_server.sendmail(gmail_user, mails[i], text)
            os.remove("/tmp/" + encde(codes[i]) + ".csv")

        cur.close()
        mail_server.close()
    except:
        e = sys.exc_info()
        logging.error(e + " !!!!!!!!!!!")


def init(db, to, code):
    try:
        today = time.strftime("%m/%d/%Y")
        mail_server = SMTP("smtp.gmail.com", 587)
        gmail_user = db.mail
        gmail_pwd = db.pwd

        msg = MIMEMultipart()
        msg['From'] = gmail_user
        msg['To'] = ", ".join([to])
        msg['Subject'] = "Init bot message"

        msg.attach(MIMEText("Добрый день. \nНачиная с "+ today +" я буду присылать вам отзывы," +
                            "которые будут приходить на ваш код - "+str(code)+".", 'plain'))

        mail_server.ehlo()
        mail_server.starttls()
        mail_server.ehlo()

        text = msg.as_string()

        mail_server.login(gmail_user, gmail_pwd)
        mail_server.sendmail(gmail_user, to, text)

        mail_server.close()
    except:
        e = sys.exc_info()
        logging.error(e)
