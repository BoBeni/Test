# -*- coding: utf-8 -*-

import logging
import re

from admin import get_admin_markup
from form_panel import get_reply_anon, get_reply_markup


def start(bot, update):
    update.message.reply_text('Привет!\nТебя привествует твой помощник. Буду рад тебе помочь.\nНе знаешь с чего ' +
                              'начать? Введи /help.')


def reply(bot, update, db):
    chart = update.message.chat.id
    mess_id = update.message.message_id
    typ = db.form_answer(chart)
    if typ == 1:
        db.save_form(chart, update.message.text)
        bot.edit_message_reply_markup(
            chat_id=chart,
            message_id=db.get_from(chart),
            reply_markup=None)
        bot.deleteMessage(chart, mess_id - 1)
        bot.sendMessage(chat_id=chart, text="Спасибо:)\nПри желании можешь указать своё имя.",
                        reply_markup=get_reply_anon())
    if typ == 0:
        try:
            code = int(update.message.text)
            if db.valid_code(code):
                db.create_form(update.message.chat.id, code)
                update.message.reply_text('Оцени урок:', reply_markup=get_reply_markup('rate'))
            else:
                update.message.reply_text("Неверный номер. Начни занаво.")
            db.delete_message(update.message.chat.id, typ)
        except ValueError:
            update.message.reply_text('Код состоит из цифр, повтори попытку.')

    if typ == 2:
        if db.valid(update.message.text):
            text, mark = get_admin_markup()
            update.message.reply_text(text, reply_markup=mark)
        else:
            update.message.reply_text('Неверный пароль. Попробуй ещё.')
        db.delete_message(update.message.chat.id, typ)

    if typ == 3:
        mail = update.message.text
        if re.match("[^@]+@[^@]+\.[^@]+", mail):
            st = db.add_teacher(mail)
            update.message.reply_text('Код ' + st)
        else:
            update.message.reply_text('Неверная почта, повтори попытку.')
        db.delete_message(update.message.chat.id, typ)
        text, mark = get_admin_markup()
        bot.sendMessage(chat_id=chart, text=text,
                        reply_markup=mark)

    if typ == 4:
        code = update.message.text
        if db.valid_code(code):
            db.del_teacher(code)
            update.message.reply_text('Изменения приняты')
        else:
            update.message.reply_text('Код состоит из цифр, повтори попытку.')
        db.delete_message(update.message.chat.id, typ)
        text, mark = get_admin_markup()
        bot.sendMessage(chat_id=chart, text=text,
                        reply_markup=mark)


def hlp(bot, update):
    update.message.reply_text('/form - заполни форму опроса для своего преподавателя.\n' +
                              '/help - получи краткую информацию по командам.\n\n' +
                              'По техническим вопросам обращайся на почту uniumibot@gmail.com')


def error(bot, update, error):
    logging.warning('Update "%s" caused error "%s"' % (update, error))
