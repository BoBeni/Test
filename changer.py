# -*- coding: utf-8 -*-

from const import Constant
from form_panel import get_reply_markup, get_reply_wrong, get_reply_anon


def change(bot, update, db):
    query = update.callback_query

    if "rate" in query.data:
        if "_" in query.data:
            sz = int(query.data[6:])
            bot.deleteMessage(query.message.chat_id, query.message.message_id + sz)
            sz += 1
        else:
            db.save_message(query.message.chat_id, query.message.message_id, 1)
            sz = 1

        bot.edit_message_text(text='Выбрал: ' + str(query.data[0]),
                              chat_id=query.message.chat_id,
                              message_id=query.message.message_id,
                              reply_markup=get_reply_markup("rate_" + str(sz)))
        db.set_rate(query.message.chat_id, int(query.data[0]))

        if "5rate" in query.data:
            text = "Oставь коплимент учителю:)"
            reply = None
        else:
            text = "А что было не так?"
            reply = get_reply_wrong()
        bot.sendMessage(chat_id=query.message.chat_id, text=text, reply_markup=reply)

    if "end_" in query.data:
        bot.edit_message_text(chat_id=query.message.chat_id, message_id=query.message.message_id,
                              text="А что было не так? - " + Constant.get_end(query.data), reply_markup=None)
        if "other" in query.data:
            bot.sendMessage(chat_id=query.message.chat_id, text="Укажи что:")
        else:
            db.save_form(query.message.chat_id, Constant.get_end(query.data))
            bot.edit_message_reply_markup(
                chat_id=query.message.chat_id, message_id=db.get_from(query.message.chat_id),
                reply_markup=None)
            bot.sendMessage(chat_id=query.message.chat_id, text="Спасибо:)\nПри желании можешь указать своё имя.\nТелеграмм запишет его автоматически.",
                        reply_markup=get_reply_anon())

    if "anon_" in query.data:
        if "_yes" in query.data:
            db.save_name(query.message.chat_id, query.message.chat.first_name + " " + query.message.chat.last_name)
        else:
            db.save_name(query.message.chat_id, 'anon')
        bot.edit_message_text(text='Спасибо:)', chat_id=query.message.chat_id, message_id=query.message.message_id)

    if "_admin" in query.data:
        text = "!"
        if "add" in query.data:
            text="Введи почту:"
            db.save_message(query.message.chat_id, query.message.message_id, 3)
        if "del" in query.data:
            text="Введи код:"
            db.save_message(query.message.chat_id, query.message.message_id, 4)
        if "exit" in query.data:
            text="Сессия закрыта"
        bot.edit_message_text(text=text,
                              chat_id=query.message.chat_id,
                              message_id=query.message.message_id,
                              reply_markup=None)