# -*- coding: utf-8 -*-


class Constant:
    @staticmethod
    def get_end(typ):
        if typ == 'end_boring':
            return "Скучно рассказывает"
        if typ == 'end_easy':
            return "Слишком легко"
        if typ == 'end_difficult':
            return "Слишком сложно"
        if typ == 'end_lost':
            return "Пропустил прошлый урок"
        if typ == 'end_other':
            return "Другое"
